
import './style.css'


const enterInput = document.querySelector<HTMLInputElement> ("#enter-input");
const amountInput = document.querySelector<HTMLInputElement> ("#amount-input");
const enterdisplay = document.querySelector<HTMLTableElement> ("#enterdisplay");
// const solde = document.querySelector<HTMLElement>("#solde");
const soldeAmount = document.querySelector<HTMLElement>("#solde-amount");
const form = document.querySelector<HTMLFormElement>('#enter-form')


let totalAmount=0;


type Operation = {
    titre : string;
    montant: number;
}
const data: Operation[] = [];

form?.addEventListener('submit', (event) => {
       event.preventDefault();
       
       
       if (enterInput?.value && amountInput?.value) {
        data.push({titre: enterInput.value, montant: Number(amountInput.value)});
       }
         console.log(data);
        draw()
    }) 



    function draw() {
        enterdisplay!.innerHTML = '';
    
    
        for (const item of data) {
    
            const para = document.createElement('tr');
    
            para.innerHTML = '<td style>'+item.titre +'</td>'+'<td>'+item.montant +'</td>';
            para.className = 'enter-title mb-0 text list-item'
    
            enterdisplay?.append(para);

            // const para2 = document.createElement('p')

            // para2.innerHTML = '<td>'+item.montant +'</td>';
            // para2.className = 'enter-amount mb-0 list-item'

            // enterdisplay?.append(para2);

        }
        totalAmount=totalAmount+Number(amountInput!.value);
        soldeAmount!.textContent =totalAmount.toString();
    }
