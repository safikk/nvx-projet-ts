# Application Pour Gérer son Budget



## Description

Cette application vous sert à entrer vos gains et vos dépenses afin d'avoir un visuel sur vos transactions.
Elle permet également d'avoir un solde de toutes vos transactions.


## Visuel

Ci-dessous la maquette :

![Maquette](application%20budget.png)



## Les difficultées :

La partie HTML et CSS ne m'a posé aucune difficultée par contre j'ai passé la majeur partie du temps sur la partie .ts que j'ai finalisé grâce à l'aide de Jean et de Romain (que je remercie :))



